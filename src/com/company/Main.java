package com.company;
//swapping
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int a,b,c;
        Scanner sc= new Scanner(System.in);
        a= sc.nextInt();
        b= sc.nextInt();
        System.out.println("Value of A and B before swapping =" + a+ " " + b);
        c=a;
        a=b;
        b=c;
        System.out.println("Value of A and B After swapping = " + a+ " " + b);

    }


}
